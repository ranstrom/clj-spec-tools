(ns clj-spec-tools.tools-test
  (:require [clj-spec-tools.tools :refer [apply-defaults load-spec-from-map validate-spec-from-map]]
            [clojure.spec.alpha :as s]
            [clojure.test :refer [deftest is testing]]))

(s/def ::name string?)
(s/def ::age  integer?)
(s/def ::house string?)

(s/def ::person
  (s/keys :req [::name ::age]
          :opt [::house]))

(def defaults {::house "Baggins"})

(def valid-map {:name "Bilbo" :age 111})

(def invalid-map {:name "Bilbo"})

(def unreasonably-old
  #(when (> (::age %) 1000) "Hobbits don't get that old"))

(def ask-for-birth-certificate
  #(when (> (::age %) 100) "Very old - ask for birth certificate!"))

(deftest test-load-spec-from-map
  (let [opts-with-defaults {:spec ::person :defaults defaults}
        opts {:spec ::person}]
    (testing "load-spec-from-map w/ defaluts"
      (is (= (load-spec-from-map opts-with-defaults valid-map)
             {::name "Bilbo" ::age 111 ::house "Baggins"}))
      (is (= (load-spec-from-map opts valid-map)
             {::name "Bilbo" ::age 111})))
    (testing "validate-spec-from-map"
      (let [[_ err] (validate-spec-from-map opts invalid-map)]
        (is err))
      (let [[_ err] (validate-spec-from-map opts valid-map)]
        (is (nil? err)))
      (let [opts-w-validator (assoc opts :validators [unreasonably-old])
            [_ err] (validate-spec-from-map opts-w-validator valid-map)]
        (is (nil? err)))
      (let [opts-w-validator (assoc opts :validators [unreasonably-old ask-for-birth-certificate])
            [_ err] (validate-spec-from-map opts-w-validator valid-map)]
        (is err)))))

(def defaults-cn {:enable true :nullable false})

(def defaults-pk {:dtk (name :hash)})

(def defaults-dimension {:enable     true
                         :pk         defaults-pk
                         :attributes [defaults-cn]})

(def dw-config-defaults
  {:dimensions [defaults-dimension]})

(deftest test-apply-defaults
  (testing "Apply defaults"
    (is (= {:dimensions [{:tn         "dim_inventory"
                          :name       "Inventory"
                          :enable     true
                          :pk         {:cn  "inventory_id"
                                       :nk  ["col1"]
                                       :dtk "hash"}
                          :attributes [{:cn       "col1"
                                        :dtk      "int"
                                        :nullable false
                                        :enable true}]}]}
           (apply-defaults
            dw-config-defaults
            {:dimensions [{:tn         "dim_inventory"
                           :name       "Inventory"
                           :pk         {:cn "inventory_id"
                                        :nk ["col1"]}
                           :attributes [{:cn  "col1"
                                         :dtk "int"}]}]})))))
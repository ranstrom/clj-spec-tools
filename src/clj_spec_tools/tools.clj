(ns clj-spec-tools.tools
  (:require [clojure.set :as cs]
            [clojure.spec.alpha :as s]
            [expound.alpha :as expound])
  (:import (clojure.lang ExceptionInfo)))

(defn- validation-error [msg e]
  (ex-info msg {:validate-error e}))

; START: DEFAULTS LOGIC

(defn- map->keys-with-vector-value
  [m]
  (reduce
   (fn [ks m]
     (let [v (get m 1)]
       (if (coll? v)
          ;(and (coll? v) (not (map? v)))
         (conj ks (get m 0))
         ks)))
   []
   m))

(defn- remove-default-vector-keys
  "Remove keys from default map IF: value is coll (excl. map) AND key does not exist in second map.

  Ensures:
  (def default {:a 1 :b [{:c 10}]})
  (def m {:a 2 :b []})
  (advanced-merge default m)
  -> {:a 2 :b []}
  By omitting :b from the default.
  "
  [d m]
  (if (and (map? d) (map? m))
    (apply dissoc d (cs/difference (set (map->keys-with-vector-value d))
                                   (set (keys m))))
    d))

(defn- coll-of-maps? [v]
  (if (and (coll? v) (every? map? v)) true false))

(defn- advanced-merge
  [defaults m]
  (merge-with
   (fn [m1 m2]
     (cond (map? m2) (advanced-merge m1 m2)
           (coll-of-maps? m2) (doall
                               (map
                                #(advanced-merge
                                  (if (map? m1) m1 (first m1))
                                  %)
                                m2))
           :else m2))
   (remove-default-vector-keys defaults m)
   m))

(defn apply-defaults
  [defaults x]
  ; TODO - add some validation on input, e.g. a defaults vector cannot have more than one entry in a vector
  (if defaults (advanced-merge defaults x) x))

; END: DEFAULTS LOGIC

;(defn- qualified-map->spec
;  [spec qm]
;  (let [ms (s/conform spec qm)]
;    (when (s/invalid? ms)
;      ;(throw (validation-error "Invalid spec" (s/explain-str spec qm)))
;      (throw (validation-error "Invalid spec" (expound/expound-str spec qm))))
;    ms))

(defn- ->qualified
  [spec-ns k]
  (if (and (keyword? k) (not (qualified-keyword? k)))
    (keyword (str spec-ns "/" (name k)))
    k))

(defn- qualify-map
  ([spec-ns m]
   (qualify-map spec-ns m nil))
  ([spec-ns m exclude-fn]
   (reduce-kv
    (fn [acc k v]
      (assoc acc (->qualified spec-ns k)
             (cond
               (and (map? v) (or (nil? exclude-fn) (exclude-fn v))) (qualify-map spec-ns v exclude-fn)
               (map? v) v
               (coll-of-maps? v) (vec (map #(qualify-map spec-ns % exclude-fn) v))
               :else v)))
    {} m)))

(defn- map->qualified
  "Add namespace from spec to existing map."
  ([spec m] (map->qualified spec m nil))
  ([spec m exclude-fn] (qualify-map (namespace spec) m exclude-fn)))

(defn- validate-qualified-map
  [spec qm]
  (when-not (s/valid? spec qm)
    (throw (validation-error "Invalid spec" (expound/expound-str spec qm))))
  qm)

(defn- validate-spec->spec
  "Takes vector of validator functions and returns first non-nil value."
  [validators ms]
  (when-let [e (some #(when-let [v (apply % [ms])] v) validators)]
    (throw (validation-error "Rule validator failure" (str e))))
  ms)

(defn load-spec-from-map
  [{:keys [spec validators defaults qualify-exception-fn]} m]
  (->> (map->qualified spec m qualify-exception-fn)
       (apply-defaults defaults)
       (validate-qualified-map spec)
       (validate-spec->spec validators)))

(defn validate-spec-from-map
  [inputs m]
  (try
    (when (load-spec-from-map inputs m)
      [{:validate-info "Validation successful."} nil])
    (catch ExceptionInfo e
      [nil e])
    (catch Exception e
      [nil e])))